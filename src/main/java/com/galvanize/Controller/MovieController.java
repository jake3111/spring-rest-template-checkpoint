package com.galvanize.Controller;

import com.galvanize.Domain.PostReviewResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.*;
import com.galvanize.Domain.Movie;
import com.galvanize.Domain.PostReviewRequest;
import com.galvanize.Domain.Review;
import com.galvanize.Service.MovieService;

import java.util.List;
import java.util.Map;

@RestController
@EnableJpaRepositories("spring.omdb.movie.exercise.Repository")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @GetMapping("/movies")
    public List<Movie> countWords(@RequestParam String q){
        return movieService.getMessage(q);
    }

    @PostMapping("/reviews")
    public PostReviewResponse postReview(@RequestBody PostReviewRequest postReviewRequest){
        return movieService.postReview(postReviewRequest);
    }

    @GetMapping("/reviews/{id}")
    public PostReviewResponse getMovieReview(@PathVariable(value = "id") Long reviewId){
        return movieService.getReview(reviewId);
    }
}
