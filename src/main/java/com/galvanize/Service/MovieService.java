package com.galvanize.Service;

import com.galvanize.Repository.MovieRepository;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.galvanize.Domain.*;
import com.galvanize.Repository.ReviewRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;

import org.springframework.http.HttpHeaders;
import java.util.List;

@Service
public class MovieService {
    private final RestTemplate restTemplate = new RestTemplate(); // <------ #2

    private final ReviewRepository reviewRepository;

    private final MovieRepository movieRepository;

    @Value("${movies.url}")
    private String galvanizeRequestUrl;

    @Value("${movies.token}")
    private String requestToken;

    public MovieService(ReviewRepository reviewRepository, MovieRepository movieRepository){
        this.reviewRepository = reviewRepository;
        this.movieRepository = movieRepository;
    }

    public List<Movie> getMessage(String sRequest) {
        StringBuilder requestString = new StringBuilder("http://www.omdbapi.com/?s=");
        requestString.append(sRequest);
        requestString.append("&apikey=e480ebad");
        String response = this.restTemplate.getForObject( // <-// ----- #3
                requestString.toString(),
                String.class
        );
        Gson gson = new Gson();
        SearchResult searchResult = gson.fromJson(response, SearchResult.class);
        return searchResult.getSearch();
    }

    public PostReviewResponse getReview(long reviewId) {
        Review getReview = reviewRepository.findById(reviewId)
                .orElseThrow(() -> new ResourceNotFoundException("Review not found for this id :: " + reviewId));
        Movie getMovie = movieRepository.findById(getReview.getMovieId())
                .orElseThrow(() -> new ResourceNotFoundException("Movie not found for this id :: " + getReview.getMovieId()));

        MovieSearchResponse movieSearchResponse = new MovieSearchResponse(getMovie.getTitle(), Integer.parseInt(getMovie.getYear()));
        movieSearchResponse.setId(getMovie.getImdbID());
        return new PostReviewResponse(movieSearchResponse, getReview);
    }

    public PostReviewResponse postReview(PostReviewRequest postReviewRequest){
        //getting the movie
        StringBuilder requestUrl = new StringBuilder("http://localhost:9090/?title=");
        requestUrl.append(postReviewRequest.getTitle());
        requestUrl.append("&year=");
        requestUrl.append(postReviewRequest.getYear());
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer 18394928-6684-43ca-9026-e19ed3028b17");
        HttpEntity entity = new HttpEntity(headers);

        ResponseEntity<MovieSearchResponse> searchResponse = restTemplate.exchange(requestUrl.toString(), HttpMethod.GET, entity, MovieSearchResponse.class);

        Review movieReview = null;
        if (searchResponse != null){
            //taking the movie id and creating a review to save to the repo
            movieReview = new Review(Long.parseLong(searchResponse.getBody().getId()),postReviewRequest.getReviewer(),postReviewRequest.getComment(), postReviewRequest.getStarRating());
            reviewRepository.save(movieReview);
        } else {
            //time to hit the post endpoint.
            String postRequestUrl = "http://localhost:9090/movies";
            MovieSearchResponse movieSearchResponse = new MovieSearchResponse(searchResponse.getBody().getTitle(), searchResponse.getBody().getYear());
            ResponseEntity<MovieSearchResponse> postMoviePathResponse = restTemplate.postForEntity(postRequestUrl, movieSearchResponse, MovieSearchResponse.class);
            StringBuilder postRedirectUrl = new StringBuilder("http://localhost:9090");
            postRedirectUrl.append(postMoviePathResponse.getBody().getPath());
            postRedirectUrl.append("&apikey=e480ebad");
            searchResponse = restTemplate.exchange(postRedirectUrl.toString(), HttpMethod.GET, entity, MovieSearchResponse.class);
            movieReview = new Review(Long.parseLong(searchResponse.getBody().getId()), postReviewRequest.getReviewer(), postReviewRequest.getComment(), postReviewRequest.getStarRating());
            reviewRepository.save(movieReview);
        }
        return new PostReviewResponse(searchResponse.getBody(), movieReview);
    }
}
