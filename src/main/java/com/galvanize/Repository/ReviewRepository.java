package com.galvanize.Repository;

import org.springframework.data.repository.CrudRepository;
import com.galvanize.Domain.Review;

public interface ReviewRepository extends CrudRepository<Review, Long> {
}
