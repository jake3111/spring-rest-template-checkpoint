package com.galvanize.Domain;

public class MovieSearchResponse {


    private String id;
    private String title;
    private String path;

    private int year;


    public MovieSearchResponse(String title, int year) {
        this.title = title;
        this.year = year;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}



/*
Sample success payload:

{
		"Title":"Gremlins",
		"Year":"1984",
		"Rated":"PG",
		"Released":"08 Jun 1984",
		"Runtime":"106 min",
		"Genre":"Comedy, Fantasy, Horror",
		"Director":"Joe Dante",
		"Writer":"Chris Columbus",
		"Actors":"Hoyt Axton, John Louie, Keye Luke, Don Steele",
		"Plot":"A boy inadvertently breaks three important rules concerning his new pet and unleashes a horde of malevolently mischievous monsters on a small town.",
		"Language":"English, Spanish",
		"Country":"USA",
		"Awards":"8 wins & 6 nominations.",
		"Poster":"https://m.media-amazon.com/images/M/MV5BZDVjN2FkYTQtNTBlOC00MjM5LTgzMWEtZWRlNGUyYmNiOTFiXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg",
		"Ratings":[
	.	{
		"Source":"Internet Movie Database",
		"Value":"7.3/10"
	.	},
	.	{
		"Source":"Rotten Tomatoes",
		"Value":"85%"
	.	},
	.	{
		"Source":"Metacritic",
		"Value":"70/100"
	.	}
		],
		"Metascore":"70",
		"imdbRating":"7.3",
		"imdbVotes":"192,656",
		"imdbID":"tt0087363",
		"Type":"movie",
		"DVD":"N/A",
		"BoxOffice":"N/A",
		"Production":"Warner Brothers, Amblin Entertainment",
		"Website":"N/A",
		"Response":"True"
}
 */