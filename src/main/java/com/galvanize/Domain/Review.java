package com.galvanize.Domain;

import javax.persistence.*;

@Entity
@Table(name = "reviews")
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    Long movieId;
    String reviewer;
    String comment;
    double starRating;

    public Review(Long movieId, String reviewer, String comment, double starRating) {
        this.movieId = movieId;
        this.reviewer = reviewer;
        this.comment = comment;
        this.starRating = starRating;
    }

    public Long getId() {
        return id;
    }

    public Long getMovieId() {
        return movieId;
    }

    public String getReviewer() {
        return reviewer;
    }

    public String getComment() {
        return comment;
    }

    public double getStarRating() {
        return starRating;
    }
}
