package com.galvanize.Domain;

public class PostReviewResponse {

    private MovieSearchResponse movie;
    private Review review;

    public PostReviewResponse(MovieSearchResponse movie, Review review) {
        this.movie = movie;
        this.review = review;
    }

    public MovieSearchResponse getMovie() {
        return movie;
    }

    public void setMovie(MovieSearchResponse movie) {
        this.movie = movie;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }
}
