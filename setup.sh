#!/bin/sh

_reset_db () {
    mysql -hdb -uroot -ppassword -P3306 -e "drop database if exists $1;"
    mysql -hdb -uroot -ppassword -P3306 -e "create database $1;"
}

_reset_db rest_template_reviews_test
_reset_db rest_template_reviews

movies/setup.sh
