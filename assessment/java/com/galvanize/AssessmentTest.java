package com.galvanize;

import com.google.gson.JsonObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Random;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {
        "movies.url=https://movies.test.host",
        "movies.token=abc123",
})
public class AssessmentTest extends GalvanizeApiTestHelpers {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ReviewRepository repo;

    private static final long MOVIE_ID = new Random().nextLong();
    private static final String MOVIE_NAME = "Die Hard";
    private static final String ENCODED_MOVIE_NAME = "Die%20Hard";
    private static final int MOVIE_YEAR = 1988;
    private static final String MOVIE_JSON_STRING = String.format(
            "{\"id\":%d,\"title\":\"%s\",\"year\":%d}", MOVIE_ID, ENCODED_MOVIE_NAME, MOVIE_YEAR);

    private static final String REVIEWER = "Hans Gruber";
    private static final String REVIEW_COMMENT = "Hated it!";
    private static final double STAR_RATING = 1;

    private static final JsonObject REVIEW_REQUEST = new JsonObject();
    private static final Review REVIEW = new Review();

    private MockRestServiceServer server;

    @Before
    public void setup() {
        server = MockRestServiceServer.createServer(restTemplate);

        REVIEW_REQUEST.addProperty("title", MOVIE_NAME);
        REVIEW_REQUEST.addProperty("year", MOVIE_YEAR);
        REVIEW_REQUEST.addProperty("reviewer", REVIEWER);
        REVIEW_REQUEST.addProperty("comment", REVIEW_COMMENT);
        REVIEW_REQUEST.addProperty("starRating", STAR_RATING);

        REVIEW.setMovieId(MOVIE_ID);
        REVIEW.setReviewer(REVIEWER);
        REVIEW.setComment(REVIEW_COMMENT);
        REVIEW.setStarRating(STAR_RATING);
    }

    @Test
    public void testCreateWithExistingMovie() {
        // given
        server.expect(requestTo(
                String.format("https://movies.test.host/movies/find?title=%s&year=%d", ENCODED_MOVIE_NAME, MOVIE_YEAR)))
                .andExpect(header("Authorization", "Bearer abc123"))
                .andRespond(withSuccess(MOVIE_JSON_STRING, MediaType.APPLICATION_JSON))
        ;

        // when
        JsonObject response = post("/reviews", REVIEW_REQUEST).getAsJsonObject();

        // then
        JsonObject movie = response.getAsJsonObject("movie");
        JsonObject review = response.getAsJsonObject("review");

        assertThat(movie.get("id").getAsLong(), equalTo(MOVIE_ID));
        assertThat(review.get("id"), notNullValue());

        server.verify();
    }

    @Test
    public void testCreateWithMissingMovie() {
        // given
        server.expect(requestTo(
                String.format("https://movies.test.host/movies/find?title=%s&year=%d", ENCODED_MOVIE_NAME, MOVIE_YEAR)))
                .andExpect(header("Authorization", "Bearer abc123"))
                .andRespond(withStatus(HttpStatus.NOT_FOUND));

        server.expect(requestTo("https://movies.test.host/movies"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(header("Content-Type", MediaType.APPLICATION_JSON_VALUE))
                .andExpect(header("Authorization", "Bearer abc123"))
                .andExpect(MockRestRequestMatchers.jsonPath("$.title", equalTo(MOVIE_NAME)))
                .andExpect(MockRestRequestMatchers.jsonPath("$.year", equalTo(MOVIE_YEAR)))
                .andRespond(withStatus(HttpStatus.FOUND).location(
                        URI.create(String.format("https://movies.test.host/movies/%d/%s", MOVIE_YEAR, ENCODED_MOVIE_NAME))));

        server.expect(requestTo(String.format("https://movies.test.host/movies/%d/%s", MOVIE_YEAR, ENCODED_MOVIE_NAME)))
                .andExpect(header("Authorization", "Bearer abc123"))
                .andRespond(withSuccess(MOVIE_JSON_STRING, MediaType.APPLICATION_JSON));

        // when
        JsonObject response = post("/reviews", REVIEW_REQUEST).getAsJsonObject();

        // then
        JsonObject movie = response.getAsJsonObject("movie");
        JsonObject review = response.getAsJsonObject("review");

        assertThat(movie.get("id").getAsLong(), equalTo(MOVIE_ID));
        assertThat(review.get("id"), notNullValue());

        server.verify();
    }

    @Test
    public void testGetWithExistingReview() {
        // given
        server.expect(requestTo(String.format("https://movies.test.host/movies/%d", MOVIE_ID)))
                .andExpect(header("Authorization", "Bearer abc123"))
                .andRespond(withSuccess(MOVIE_JSON_STRING, MediaType.APPLICATION_JSON));
        Review expected_review = repo.save(REVIEW);

        // when
        JsonObject response = get("/reviews/" + expected_review.getId()).getAsJsonObject();

        // then
        JsonObject movie = response.getAsJsonObject("movie");
        JsonObject review = response.getAsJsonObject("review");

        assertThat(movie.get("id").getAsLong(), equalTo(MOVIE_ID));
        assertThat(review.get("id").getAsLong(), equalTo(expected_review.getId()));
    }

    @Test
    public void testGetWithMissingReview() {
        // given
        repo.deleteAll();

        // when
        ResponseEntity<String> response = requestWithoutBody("/reviews/1", HttpMethod.GET);

        // then
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), nullValue());
    }
}
