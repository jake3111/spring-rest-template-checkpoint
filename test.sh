#!/bin/sh

# filter output created when the spring shutdown hooks are called
# as these are outside the test process and doesn't follow the test
# logging rules. TODO: use a more idomatic way to do this

./gradlew --console=plain :clean :assess 2>&1 | grep -v extShutdownHook

# For more detail, run "./gradlew assess --info"
